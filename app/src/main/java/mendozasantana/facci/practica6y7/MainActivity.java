package mendozasantana.facci.practica6y7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button sensor, vibrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensor = findViewById(R.id.BTNsensor);
        vibrar = findViewById(R.id.BTNVibrar);

        sensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(
                        MainActivity.this, ActivitySensor.class);
                startActivity(intent);
            }
        });
        vibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, ActivityVibrar.class);
                startActivity(intent);
            }
        });
    }
}
